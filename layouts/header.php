
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="index.php" class="logo">
                InventarisirSMK
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo isset($_SESSION['login_admin'])?$_SESSION['nama_petugas']:(isset($_SESSION['login_operator'])?$_SESSION['nama_petugas']:$_SESSION['nama_pegawai']);?>&nbsp;<i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <?php
                                      if(isset($_SESSION['login_admin'])){
                                    ?>
                                        <img src="../assets/img/avatar5.png" class="img-circle" alt="User Image" />
                                    <?php
                                      }else if(isset($_SESSION['login_operator'])){
                                    ?>
                                        <img src="../assets/img/avatar3.png" class="img-circle" alt="User Image" />
                                    <?php
                                      }else if(isset($_SESSION['login_peminjam'])){
                                    ?>
                                        <img src="../assets/img/avatar04.png" class="img-circle" alt="User Image" />
                                    <?php
                                    }
                                    ?>
                                    <?php
                                      if(isset($_SESSION['login_admin'])){
                                    ?>
                                    <p>
                                        <?php echo isset($_SESSION['login_admin'])?$_SESSION['nama_petugas']:(isset($_SESSION['login_operator'])?$_SESSION['nama_petugas']:$_SESSION['nama_pegawai']);?>
                                        <small><?php echo isset($_SESSION['login_admin'])?$_SESSION['level']:(isset($_SESSION['login_operator'])?$_SESSION['level']:$_SESSION['nama_pegawai']);?>&nbsp;-&nbsp;Since Jan. 2019</small>
                                    </p>
                                    <?php
                                      }else if(isset($_SESSION['login_operator'])){
                                    ?>
                                    <p>
                                        <?php echo isset($_SESSION['login_admin'])?$_SESSION['nama_petugas']:(isset($_SESSION['login_operator'])?$_SESSION['nama_petugas']:$_SESSION['nama_pegawai']);?>
                                        <small><?php echo isset($_SESSION['login_admin'])?$_SESSION['level']:(isset($_SESSION['login_operator'])?$_SESSION['level']:$_SESSION['nama_pegawai']);?>&nbsp;-&nbsp;Since Jan. 2019</small>
                                    </p>
                                    <?php
                                      }else if(isset($_SESSION['login_peminjam'])){
                                    ?>
                                    <p><?php echo isset($_SESSION['login_admin'])?$_SESSION['nama_petugas']:(isset($_SESSION['login_operator'])?$_SESSION['nama_petugas']:$_SESSION['nama_pegawai']);?>
                                        <small>NIP&nbsp;-&nbsp;<?php echo isset($_SESSION['login_admin'])?$_SESSION['nama_petugas']:(isset($_SESSION['login_operator'])?$_SESSION['nama_petugas']:$_SESSION['nip']);?>
                                        </small>
                                    </p>
                                    <?php
                                    }
                                    ?>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <?php
                                      if(isset($_SESSION['login_admin'])){
                                    ?>
                                    <div align="center">
                                        <a href="../logout.php" onclick="return confirm('Yakin ingin keluar?')" class="btn btn-default btn-flat">Log out</a>
                                    </div>
                                    <?php
                                      }else if(isset($_SESSION['login_operator'])){
                                    ?>
                                    <div align="center">
                                        <a href="../logout.php" onclick="return confirm('Yakin ingin keluar?')" class="btn btn-default btn-flat">Log out</a>
                                    </div>
                                    <?php
                                      }else if(isset($_SESSION['login_peminjam'])){
                                    ?>
                                    <div align="center">
                                        <a href="../logout.php" onclick="return confirm('Yakin ingin keluar?')" class="btn btn-default btn-flat">Log out</a>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>