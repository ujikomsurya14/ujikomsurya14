

        <div class="wrapper row-offcanvas row-offcanvas-left">

            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                        <?php
                          if(isset($_SESSION['login_admin'])){
                        ?>
                            <img src="../assets/img/avatar5.png" class="img-circle" alt="User Image" />
                        <?php
                          }else if(isset($_SESSION['login_operator'])){
                        ?>
                            <img src="../assets/img/avatar3.png" class="img-circle" alt="User Image" />

                        <?php
                          }else if(isset($_SESSION['login_peminjam'])){
                        ?>
                            <img src="../assets/img/avatar04.png" class="img-circle" alt="User Image" />
                        <?php
                    }?>
                        </div>
                        <div class="pull-left info">
                        <?php
                          if(isset($_SESSION['login_admin'])){
                        ?>
                            <p>Hai,<?php echo isset($_SESSION['login_admin'])?$_SESSION['nama_petugas']:(isset($_SESSION['login_operator'])?$_SESSION['nama_petugas']:$_SESSION['nama_pegawai']);?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i>&nbsp;Status Level -&nbsp;<?php echo isset($_SESSION['login_admin'])?$_SESSION['level']:(isset($_SESSION['login_operator'])?$_SESSION['level']:$_SESSION['nama_pegawai']);?></a>
                        <?php
                          }else if(isset($_SESSION['login_operator'])){
                        ?>
                            <p>Hai,<?php echo isset($_SESSION['login_admin'])?$_SESSION['nama_petugas']:(isset($_SESSION['login_operator'])?$_SESSION['nama_petugas']:$_SESSION['nama_pegawai']);?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i>&nbsp;Status Level -&nbsp;<?php echo isset($_SESSION['login_admin'])?$_SESSION['nama_petugas']:(isset($_SESSION['login_operator'])?$_SESSION['level']:$_SESSION['nama_pegawai']);?></a>
                        <?php
                          }else if(isset($_SESSION['login_peminjam'])){
                        ?>
                            <p>Hai,<?php echo isset($_SESSION['login_admin'])?$_SESSION['nama_petugas']:(isset($_SESSION['login_operator'])?$_SESSION['nama_petugas']:$_SESSION['nama_pegawai']);?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i>&nbsp;NIP -&nbsp;<?php echo isset($_SESSION['login_admin'])?$_SESSION['nama_petugas']:(isset($_SESSION['login_operator'])?$_SESSION['level']:$_SESSION['nip']);?></a>
                            <?php
                        }?>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="index.php">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <?php
                          if(isset($_SESSION['login_admin'])){
                        ?>
                        <li>
                            <a href="data_barang.php">
                                <i class="fa fa-bar-chart-o"></i> <span>Inventarisir</span>
                            </a>
                        </li>
                        <li>
                            <a href="Peminjaman.php">
                                <i class="fa fa-share"></i> <span>Peminjaman</span>
                            </a>
                        </li>
                        <li>
                            <a href="Pengembalian.php">
                                <i class="fa fa-reply"></i> <span>Pengembalian</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-folder-open"></i> <span>Master Data</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="data_petugas.php"><i class="fa fa-angle-right"></i>Data Petugas</a></li>
                                <li><a href="data_pegawai.php"><i class="fa fa-angle-right"></i>Data Pegawai</a></li>
                                <li><a href="data_jenis.php"><i class="fa fa-angle-right"></i>Data Jenis</a></li>
                                <li><a href="data_ruang.php"><i class="fa fa-angle-right"></i>Data Ruangan</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="laporan_peminjaman.php">
                                <i class="fa fa-file"></i> <span>Laporan</span>
                            </a>
                        </li>
                        <?php
                          }else if(isset($_SESSION['login_operator'])){
                        ?>
                        <li>
                            <a href="Peminjaman.php">
                                <i class="fa fa-share"></i> <span>Peminjaman</span>
                            </a>
                        </li>
                        <li>
                            <a href="Pengembalian.php">
                                <i class="fa fa-reply"></i> <span>Pengembalian</span>
                            </a>
                        </li>
                        <?php
                          }else if(isset($_SESSION['login_peminjam'])){
                        ?>
                        <li>
                            <a href="peminjaman.php">
                                <i class="fa fa-share"></i> <span>Peminjaman</span>
                            </a>
                        </li>
                        <?php
                          }
                        ?>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>