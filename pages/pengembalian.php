<!-- Cek apakah sudah login -->
<?php
  include '../config/koneksi.php';
  session_start();
  if(isset($_SESSION['login_admin']) || isset($_SESSION['login_operator']) || isset($_SESSION['login_peminjam']) ){
?>
<!DOCTYPE html>
<html>
    <head>
    <?php include '../layouts/link.php'; ?>
    </head>
    <body class="skin-blue">
        <?php include '../layouts/header.php'; ?>
        <?php include '../layouts/navbar.php'; ?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Pengembalian
                        <small>Data pengembalian</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-bar-chart-o"></i> Home</a></li>
                        <li class="active"></i> Pengembalian</li>
                    </ol>
                </section>
                <hr>
                <!-- Semua Konten -->
                <section class="content">

                    <!-- Row Tengah -->
                    <div class="col-lg-12">

                        <!-- box -->
                        <div class="box box-solid box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-table"></i> Tabel Data Pengembalian</h3>
                                <!-- Alat box -->
                                <div class="pull-right box-tools">
                                    <button class="btn btn-primary btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-primary btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div><!-- /. alat -->
                            </div><!-- box-header -->

                            <div class="box-body">
                                <div class="table-responsive">
                                  <table id="example1" class="table table-bordered table-hover">
                                    <thead>
                                      <tr>
                                          <th class="text-center tableNumber">No.</th>
                                          <th class="text-center">Kode Barang</th>
                                          <th class="text-center">Nama Barang</th>
                                          <th class="text-center">Nama Peminjam</th>
                                          <th class="text-center">Tanggal Kembali</th>
                                          <th class="text-center">Jumlah Barang</th>
                                          <th class="text-center">Status Peminjaman</th>
                                          <th class="text-center">Opsi</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                      include("../config/koneksi.php");
                                      $no=0;
                                      if(isset($_SESSION['login_admin'])){
                                        $where="p.id_petugas=".$_SESSION['login_admin'];
                                        $nama_petugas=$_SESSION['nama_petugas'];
                                      }else if(isset($_SESSION['login_operator'])){
                                        $where="p.id_petugas=".$_SESSION['login_operator'];
                                        $nama_petugas=$_SESSION['nama_petugas'];
                                      }else if(isset($_SESSION['login_peminjam'])){
                                        $where="p.id_pegawai=".$_SESSION['login_peminjam'];
                                        $nama_petugas=$_SESSION['nama_pegawai'];
                                      }
                                      $query = mysqli_query($koneksi,"SELECT p.*,pt.nama_petugas,i.nama_barang,i.kode_barang,d.jumlah,i.id_invent FROM table_peminjaman p LEFT JOIN table_detail d ON p.id_peminjaman=d.id_peminjaman LEFT JOIN table_petugas pt ON p.id_petugas=pt.id_petugas LEFT JOIN table_invent i ON d.id_invent=i.id_invent WHERE $where ORDER BY p.id_peminjaman ASC") or die (mysqli_error($koneksi));
                                      if (mysqli_num_rows($query) == 0) {
                                          echo '<tr><td class="text-center" colspan="8">Belum ada Peminjaman!</td></tr>';
                                      }else{
                                          while ($data = mysqli_fetch_array($query)) {
                                          $no++;
                                    ?>
                                    <tr>
                                        <td class="text-center"><?php echo $no.'.'; ?></td>          
                                        <td class="text-center"><?php echo $data['kode_barang']; ?></td>
                                        <td><?php echo $data['nama_barang']; ?></td>
                                        <td><?php echo $nama_petugas ?></td>
                                        <td class="text-center"><?php echo $data['tgl_kembali']; ?></td>
                                        <td class="text-center"><?php echo $data['jumlah']; ?></td>
                                        <td class="text-center">
                                          <?php
                                            if($data['status_peminjaman']=='Sedang Dipinjam'){
                                              echo "Belum Dikembalikan";
                                            }else if($data['status_peminjaman']=='Telah Dikembalikan'){
                                              echo $data['status_peminjaman'];
                                            }
                                          ?>
                                        </td>
                                        <?php
                                          if($data['status_peminjaman']=='Sedang Dipinjam'){
                                        ?>
                                        <td class="text-center">
                                          <a href="crud/proses_pengembalian_barang.php?id_petugas<?=$data['id_petugas'];?>&id_peminjaman=<?=$data['id_peminjaman'];?>&id_invent=<?=$data['id_invent'];?>&jumlah=<?=$data['jumlah'];?>" class="label label-danger"><i class="fa fa-arrow-left"></i> Kembalikan</a>
                                        </td>
                                        <?php 
                                        }else{
                                          echo "<td></td>";
                                        ?>
                                        <?php
                                        }
                                        ?>
                                    </tr>
                                    <?php
                                    }
                                    }
                                    ?>
                                    </tbody>  
                                  </table>
                                </div>
                            </div><!-- box-body -->
                        </div><!-- /.box  -->


                    </div><!-- /.col-12 -->

                </section><!-- /.content (semua konten) -->

            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <?php include '../layouts/script.php'; ?>
        <!-- jQuery 3.2.1 -->
        <script src="../assets/jquery-3.2.1.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="../assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="../assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- Memanggil Autocomplete.js -->
        <script src="assets/js/jquery.autocomplete.min.js"></script>
        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
    </body>
</html>
<?php
}else{
  if(isset($_SESSION['login_peminjam'])){
    echo"<script>window.location.assign('../login2.php');</script>";
  }else{
    echo"<script>window.location.assign('../login.php');</script>";
  }
}
?>