<!-- Cek apakah sudah login -->
<?php
  include '../../config/koneksi.php';
  session_start();
  if(isset($_SESSION['login_admin']) || isset($_SESSION['login_operator']) || isset($_SESSION['login_peminjam']) ){
?>
<!DOCTYPE html>
<html>
<head>
        <meta charset="UTF-8">
        <title>SMKN 1 CIOMAS</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="../../assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="../../assets/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- daterange picker -->
        <link href="../../assets/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../../assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="../../assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- ICOM -->
        <link href='../../assets/img/icon.png' rel='shortcut icon'>
</head>
<body>
	<div class="container">
		<h2 align="center" id="title-laporan"><b><u>DATA LAPORAN PEMINJAMAN</u></b></h2><br>
        <a class="btn btn-primary no-print" onclick="window.print()"><i class="fa fa-print"></i> Cetak</a>
		<a  onclick="self.close()" type="button" class="no-print btn btn-default"><i class="fa fa-times"></i> Batal</a>
		<br><br>
		<p>Laporan pada tanggal : <?php echo date("d/m/Y"); ?></p>
		<table border="1" width="100%" style="border-collapse: collapse;">
        	<thead>
        	  <tr>
        	      <th class="text-center tableNumber">No.</th>
        	      <th class="text-center">Kode Barang</th>
        	      <th class="text-center">Nama Barang</th>
        	      <th class="text-center">Nama Peminjam</th>
        	      <th class="text-center">Tanggal Pinjam</th>
        	      <th class="text-center">Tanggal Kembali</th>
        	      <th class="text-center">Jumlah Barang</th>
        	      <th class="text-center">Status Peminjaman</th>
        	  </tr>
        	</thead>
        	<tbody>
        	<?php
        	  include("../../config/koneksi.php");
        	  $no=0;
        	  if(isset($_SESSION['login_admin'])){
        	    $where="p.id_petugas=".$_SESSION['login_admin'];
        	    $nama_petugas=$_SESSION['nama_petugas'];
        	  }else if(isset($_SESSION['login_operator'])){
        	    $where="p.id_petugas=".$_SESSION['login_operator'];
        	    $nama_petugas=$_SESSION['nama_petugas'];
        	  }else if(isset($_SESSION['login_peminjam'])){
        	    $where="p.id_pegawai=".$_SESSION['login_peminjam'];
        	    $nama_petugas=$_SESSION['nama_pegawai'];
        	  }
        	  $query = mysqli_query($koneksi,"SELECT p.*,pt.nama_petugas,i.nama_barang,i.kode_barang,d.jumlah,i.id_invent FROM table_peminjaman p LEFT JOIN table_detail d ON p.id_peminjaman=d.id_peminjaman LEFT JOIN table_petugas pt ON p.id_petugas=pt.id_petugas LEFT JOIN table_invent i ON d.id_invent=i.id_invent WHERE status_peminjaman='Telah Dikembalikan' ORDER BY p.id_peminjaman ASC") or die (mysqli_error($koneksi));
        	  if (mysqli_num_rows($query) == 0) {
        	      echo '<tr><td class="text-center" colspan="8">Belum ada Peminjaman!</td></tr>';
        	  }else{
        	      while ($data = mysqli_fetch_array($query)) {
        	      $no++;
        	?>
        	<tr>
        	    <td class="text-center"><?php echo $no.'.'; ?></td>          
        	    <td class="text-center"><?php echo $data['kode_barang']; ?></td>
        	    <td class="text-center"><?php echo $data['nama_barang']; ?></td>
        	    <td class="text-center"><?php echo $nama_petugas ?></td>
        	    <td class="text-center"><?php echo $data['tgl_pinjam']; ?></td>
        	    <td class="text-center"><?php echo $data['tgl_kembali']; ?></td>
        	    <td class="text-center"><?php echo $data['jumlah']; ?></td>
        	    <td class="text-center"><?php echo $data['status_peminjaman']; ?></td>
        	</tr>
        	<?php
        	}
        	}
        	?>
        	</tbody>  
		</table>
	</div>
</body>
</html>
<?php
}else{
  if(isset($_SESSION['login_peminjam'])){
    echo"<script>window.location.assign('../login2.php');</script>";
  }else{
    echo"<script>window.location.assign('../login.php');</script>";
  }
}
?>