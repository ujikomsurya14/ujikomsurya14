<!-- Cek apakah sudah login -->
<?php
  include '../../config/koneksi.php';
  session_start();
  if(isset($_SESSION['login_admin']) || isset($_SESSION['login_operator']) || isset($_SESSION['login_peminjam']) ){

  	header("Content-type: application/vnd-ms-excel");
	// Mendefinisikan nama file ekspor "hasil-export.xls"
	header("Content-Disposition: attachment; filename=laporan_peminjaman.xls");
 
	// Tambahkan table
?>
<!DOCTYPE html>
<html>
<head>
	
</head>
<body>
<h2 align="center" id="title-laporan"><b><u>DATA LAPORAN PEMINJAMAN</u></b></h2><br>
<p>Laporan pada tanggal : <?php echo date("d/m/Y"); ?></p>
<table border="1" width="100%" style="border-collapse: collapse;">
        	<thead>
        	  <tr>
        	      <th align="center">No.</th>
        	      <th align="center">Kode Barang</th>
        	      <th align="center">Nama Barang</th>
        	      <th align="center">Nama Peminjam</th>
        	      <th align="center">Tanggal Pinjam</th>
        	      <th align="center">Tanggal Kembali</th>
        	      <th align="center">Jumlah Barang</th>
        	      <th align="center">Status Peminjaman</th>
        	  </tr>
        	</thead>
        	<tbody>
        	<?php
        	  include("../../config/koneksi.php");
        	  $no=0;
        	  if(isset($_SESSION['login_admin'])){
        	    $where="p.id_petugas=".$_SESSION['login_admin'];
        	    $nama_petugas=$_SESSION['nama_petugas'];
        	  }else if(isset($_SESSION['login_operator'])){
        	    $where="p.id_petugas=".$_SESSION['login_operator'];
        	    $nama_petugas=$_SESSION['nama_petugas'];
        	  }else if(isset($_SESSION['login_peminjam'])){
        	    $where="p.id_pegawai=".$_SESSION['login_peminjam'];
        	    $nama_petugas=$_SESSION['nama_pegawai'];
        	  }
        	  $query = mysqli_query($koneksi,"SELECT p.*,pt.nama_petugas,i.nama_barang,i.kode_barang,d.jumlah,i.id_invent FROM table_peminjaman p LEFT JOIN table_detail d ON p.id_peminjaman=d.id_peminjaman LEFT JOIN table_petugas pt ON p.id_petugas=pt.id_petugas LEFT JOIN table_invent i ON d.id_invent=i.id_invent WHERE status_peminjaman='Telah Dikembalikan' ORDER BY p.id_peminjaman ASC") or die (mysqli_error($koneksi));
        	  if (mysqli_num_rows($query) == 0) {
        	      echo '<tr><td class="text-center" colspan="8">Belum ada Peminjaman!</td></tr>';
        	  }else{
        	      while ($data = mysqli_fetch_array($query)) {
        	      $no++;
        	?>
        	<tr>
        	    <td align="center"><?php echo $no; ?></td>          
        	    <td align="center"><?php echo $data['kode_barang']; ?></td>
        	    <td align="center"><?php echo $data['nama_barang']; ?></td>
        	    <td align="center"><?php echo $nama_petugas ?></td>
        	    <td align="center"><?php echo $data['tgl_pinjam']; ?></td>
        	    <td align="center"><?php echo $data['tgl_kembali']; ?></td>
        	    <td align="center"><?php echo $data['jumlah']; ?></td>
        	    <td align="center"><?php echo $data['status_peminjaman']; ?></td>
        	</tr>
        	<?php
        	}
        	}
        	?>
        	</tbody>  
</table>
</body>
</html>
<?php
}else{
  if(isset($_SESSION['login_peminjam'])){
    echo"<script>window.location.assign('../login2.php');</script>";
  }else{
    echo"<script>window.location.assign('../login.php');</script>";
  }
}
?>