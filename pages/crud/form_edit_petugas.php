<?php
    include "../../config/koneksi.php";
	$id_petugas=$_GET['id_petugas'];
	$modal=mysqli_query($koneksi,"SELECT * FROM table_petugas WHERE id_petugas='$id_petugas'");
	while($r=mysqli_fetch_array($modal)){
?>
<div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">Edit Petugas</h4>
        </div>
        <div class="modal-body">
        	<form action="crud/proses_edit_petugas.php" name="modal_popup" enctype="multipart/form-data" method="POST">        		
                <div class="form-group">
                	<label for="nama_petugas">Nama Petugas</label>
                    <input type="hidden" name="id_petugas" class="form-control" value="<?php echo $r['id_petugas']; ?>" />
     				<input type="text" name="nama_petugas" class="form-control" value="<?php echo $r['nama_petugas']; ?>"/>
                </div>
                <div class="form-group">
                	<label for="email">Email</label>
     				<input type="email" name="email" class="form-control" value="<?php echo $r['email']; ?>"/>
                </div>
                <div class="form-group">
                	<label for="username">Username</label>
     				<input type="text" name="username" class="form-control" value="<?php echo $r['username']; ?>"/>
                </div>
                <div class="form-group">
                	<label for="password">Password</label>
     				<input type="text" name="password" class="form-control" value="<?php echo $r['password']; ?>"/>
                </div>
                <div class="form-group">
                	<label for="level">Level</label>
     				<input type="text" name="level" class="form-control" value="<?php echo $r['level']; ?>"/>
                </div>
	            <div class="modal-footer">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
	                <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i> Batal</button>
	            </div>
            </form>
            <?php } ?>
            </div>
        </div>
    </div>
</div>