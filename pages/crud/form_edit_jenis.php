<?php
    include "../../config/koneksi.php";
	$id_jenis=$_GET['id_jenis'];
	$modal=mysqli_query($koneksi,"SELECT * FROM table_jenis WHERE id_jenis='$id_jenis'");
	while($r=mysqli_fetch_array($modal)){
?>
<div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">Edit Jenis</h4>
        </div>
        <div class="modal-body">
        	<form action="crud/proses_edit_jenis.php" name="modal_popup" enctype="multipart/form-data" method="POST">        		
                <div class="form-group">
                	<label for="kode_jenis">Kode Jenis</label>
                    <input type="hidden" name="id_jenis" class="form-control" value="<?php echo $r['id_jenis']; ?>" />
     				<input type="text" name="kode_jenis" class="form-control" autocomplete="off" value="<?php echo $r['kode_jenis']; ?>"/>
                </div>
                <div class="form-group">
                	<label for="nama_jenis">Nama Jenis</label>
     				<input type="text" name="nama_jenis" class="form-control" autocomplete="off" value="<?php echo $r['nama_jenis']; ?>"/>
                </div>
                <div class="form-group">
                	<label for="keterangan">Keterangan</label>       
     				<textarea name="keterangan" class="form-control"><?php echo $r['keterangan']; ?></textarea>
                </div>
	            <div class="modal-footer">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
	                <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i> Batal</button>
	            </div>
            </form>
            <?php } ?>
            </div>
        </div>
    </div>
</div>