<?php
    include "../../config/koneksi.php";
	$id_pegawai=$_GET['id_pegawai'];
	$modal=mysqli_query($koneksi,"SELECT * FROM table_pegawai WHERE id_pegawai='$id_pegawai'");
	while($r=mysqli_fetch_array($modal)){
?>
<div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">Edit Pegawai</h4>
        </div>
        <div class="modal-body">
        	<form action="crud/proses_edit_pegawai.php" name="modal_popup" enctype="multipart/form-data" method="POST">        		
                <div class="form-group">
                    <label for="nip">NIP</label>
                    <input type="text" name="nip" class="form-control" maxlength="18" readonly value="<?php echo $r['nip']; ?>"/>
                </div>
                <div class="form-group">
                	<label for="nama_pegawai">Nama Pegawai</label>
                    <input type="hidden" name="id_pegawai" class="form-control" value="<?php echo $r['id_pegawai']; ?>" />
     				<input type="text" name="nama_pegawai" class="form-control" value="<?php echo $r['nama_pegawai']; ?>"/>
                </div>
                <div class="form-group">
                	<label for="alamat">Alamat</label>       
     				<textarea name="alamat" class="form-control"><?php echo $r['alamat']; ?></textarea>
                </div>
	            <div class="modal-footer">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
	                <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i> Batal</button>
	            </div>
            </form>
            <?php } ?>
            </div>
        </div>
    </div>
</div>