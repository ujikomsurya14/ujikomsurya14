<!-- Cek apakah sudah login -->
<?php
  include '../config/koneksi.php';
  session_start();
  if(isset($_SESSION['login_admin']) || isset($_SESSION['login_operator']) || isset($_SESSION['login_peminjam']) ){
?>
<!DOCTYPE html>
<html>
    <head>
    <?php include '../layouts/link.php'; ?>
    </head>
    <body class="skin-blue">
        <?php include '../layouts/header.php'; ?>
        <?php include '../layouts/navbar.php'; ?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Barang
                        <small>Data Barang</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-bar-chart-o"></i> Home</a></li>
                        <li class="active"></i> Barang</li>
                    </ol>
                </section>
                <hr>
                <!-- Semua Konten -->
                <section class="content">
                        <!-- box -->
                        <div class="box box-solid box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-table"></i> Tabel Data Barang</h3>
                                <!-- Alat box -->
                                <div class="pull-right box-tools">
                                    <button class="btn btn-primary btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-primary btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div><!-- /. alat -->
                            </div><!-- box-header -->
                            <div class="box-body">
                                <div class="box-tools pull-left">
                                    <a href="#" data-target="#ModalAdd" data-toggle="modal" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Barang</a>
                                </div><br><br>
                                <div class="table-responsive">
                                    <table id="example1" class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="text-center tableNumber">No.</th>
                                            <th>Kode Barang</th>
                                            <th>Nama Barang</th>
                                            <th>Jumlah Barang</th>
                                            <th>Jenis Barang</th>
                                            <th>Kondisi Barang</th>
                                            <th>Ruang</th>
                                            <th>Tanggal Register</th>
                                            <th>Keterangan</th>
                                            <th>Petugas</th>
                                            <th class="text-center tableOpsi">Opsi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                          include("../config/koneksi.php");
                                          $no=0;
                                          $query = mysqli_query($koneksi,"SELECT table_invent.*,table_jenis.nama_jenis,table_ruang.nama_ruang,table_petugas.nama_petugas FROM table_invent LEFT JOIN table_jenis ON table_invent.id_jenis=table_jenis.id_jenis LEFT JOIN table_ruang ON table_invent.id_ruang=table_ruang.id_ruang LEFT JOIN table_petugas ON table_invent.id_petugas=table_petugas.id_petugas ORDER BY tgl_register") or die (mysqli_error());
                                          if (mysqli_num_rows($query) == 0) {
                                              echo '<tr><td class="text-center" colspan="11">Tidak ada Data!</td></tr>';
                                          }else{
                                              while ($data = mysqli_fetch_array($query)) {
                                              $no++;
                                        ?>
                                        <tr>
                                            <td class="text-center"><?php echo $no; ?></td>          
                                            <td><?php echo $data['kode_barang']; ?></td>          
                                            <td><?php echo $data['nama_barang']; ?></td>
                                            <td><?php echo $data['jumlah']; ?></td>
                                            <td><?php echo $data['nama_jenis']; ?></td>
                                            <td><?php echo $data['kondisi_barang']; ?></td>
                                            <td><?php echo $data['nama_ruang']; ?></td>
                                            <td><?php echo $data['tgl_register']; ?></td>
                                            <td><?php echo $data['keterangan']; ?></td>
                                            <td><?php echo $data['nama_petugas']; ?></td>          
                                            <td class="text-center">
                                              <a class="btn btn-primary btn-xs open_modal" id='<?php echo $data['id_invent']; ?>'><i class="fa fa-edit"></i> Edit</a>
                                              <a class="btn btn-danger btn-xs" onclick="confirm_modal('crud/proses_hapus_barang.php?&id_invent=<?php echo $data['id_invent']; ?>');"><i class="fa fa-trash-o"></i> Hapus</a>
                                            </td>
                                        </tr>
                                        <?php
                                        }
                                        }
                                        ?>
                                      </tbody>  
                                    </table>
                                </div>
                              </div>
                          <!-- /.box-body -->
                        </div><!-- /.box -->
                </section><!-- /.content (semua konten) -->
              <!-- CRUD MODAL -->
              <!-- Modal Popup untuk Add--> 
              <div id="ModalAdd" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      <h4 class="modal-title" id="myModalLabel">Form Tambah Barang</h4>
                    </div>
                    <div class="modal-body">
                      <form action="crud/proses_tambah_barang.php" name="modal_popup" enctype="multipart/form-data" method="POST">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label for="kode_barang">Kode Barang</label>
                            <input type="text" name="kode_barang" class="form-control" value="<?php echo $kode_barang?>" disabled/>
                          </div>
                          <div class="form-group">
                            <label for="nama_barang">Nama Barang</label>
                            <input type="text" name="nama_barang" class="form-control" autocomplete="off" required/>
                          </div>
                          <div class="form-group">
                            <label for="jumlah">Jumlah Barang</label>
                            <input type="number" name="jumlah" class="form-control" autocomplete="off" required/>
                          </div>
                          <div class="form-group">
                            <label for="kondisi_barang">Kondisi Barang</label>
                            <input type="text" name="kondisi_barang" class="form-control" autocomplete="off" required/>
                          </div>
                          <div class="form-group">
                            <label>Jenis Barang</label>
                            <select name="nama_jenis" class="form-control" required>
                              <option selected="selected" hidden disabled="">Pilih Jenis</option>
                              <?php
                                $q=mysqli_query($koneksi,"SELECT * FROM table_jenis ORDER BY nama_jenis ASC");
                                while($show=mysqli_fetch_array($q)){
                              ?>
                              <option value="<?=$show['id_jenis'];?>"><?=$show['nama_jenis'];?></option>
                              <?php 
                              }
                              ?>
                            </select>
                          </div>       
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label>Ruang</label>
                            <select name="nama_ruang" class="form-control" required>
                              <option selected="selected" hidden>Pilih Ruang</option>
                              <?php
                                $q=mysqli_query($koneksi,"SELECT * FROM table_ruang ORDER BY nama_ruang ASC");
                                while($show=mysqli_fetch_array($q)){
                              ?>
                              <option value="<?=$show['id_ruang'];?>"><?=$show['nama_ruang'];?></option>
                              <?php 
                              }
                              ?>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="tgl_register">Tanggal Register</label>
                            <input type="text" name="tgl_register" class="form-control" value="<?php echo date('d/m/y');?>" disabled/>
                          </div>
                          <div class="form-group">
                            <label for="nama_petugas">Petugas</label>
                            <input type="hidden" name="id_petugas" value="<?php echo $_SESSION['login_admin'];?>">
                            <input type="text" name="nama_petugas" class="form-control" value="<?php echo $_SESSION['nama_petugas'];?>" disabled>
                          </div>
                          <div class="form-group">
                            <label for="keterangan">Keterangan</label>
                            <input type="text" name="keterangan" class="form-control" autocomplete="off">
                          </div>
                        </div>
                        <div class="modal-footer">
                          <div class="col-sm-12">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
                            <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i> Batal</button>
                          </div>
                        </div>
                      </form>
                    </div>   
                  </div>
                </div>
              </div>
              <!-- Modal Popup untuk Edit--> 
              <div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
              <!-- Modal Popup untuk Delete--> 
              <div class="modal fade" id="modal_delete">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title">Apa anda yakin ingin menghapusnya?</h4>
                    </div>
                    <div class="modal-footer" style="padding: 15px !important;">
                        <a class="btn btn-warning" id="delete_link"><i class="fa fa-trash-o"></i> Hapus</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
                    </div>
                  </div>  
                </div>
              </div>
              <!-- END CRUD MODAL -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <?php include '../layouts/script.php'; ?>
        <!-- Javascript untuk popup modal Edit--> 
        <script type="text/javascript">
          $(document).ready(function () {
          $(".open_modal").click(function(e) {
              var m = $(this).attr("id");
              $.ajax({
                    url: "crud/form_edit_barang.php",
                    type: "GET",
                    data : {id_invent: m,},
                    success: function (ajaxData){
                      $("#ModalEdit").html(ajaxData);
                      $("#ModalEdit").modal('show',{backdrop: 'true'});
                    }
                  });
                });
              });
        </script>
        <!-- Javascript untuk popup modal Delete--> 
        <script type="text/javascript">
            function confirm_modal(delete_url)
            {
              $('#modal_delete').modal('show', {backdrop: 'static'});
              document.getElementById('delete_link').setAttribute('url' , delete_url);
            }
            $('#delete_link').click(function(){
              window.location=$('#delete_link')[0].attributes.url.value;
            })
        </script>
        <!-- DATA TABES SCRIPT -->
        <script src="../assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="../assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>

    </body>
</html>
<?php
}else{
  if(isset($_SESSION['login_peminjam'])){
    echo"<script>window.location.assign('../login2.php');</script>";
  }else{
    echo"<script>window.location.assign('../login.php');</script>";
  }
}
?>