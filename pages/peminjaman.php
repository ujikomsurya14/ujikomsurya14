<!-- Cek apakah sudah login -->
<?php
  include '../config/koneksi.php';
  session_start();
  if(isset($_SESSION['login_admin']) || isset($_SESSION['login_operator']) || isset($_SESSION['login_peminjam']) ){
?>
<!DOCTYPE html>
<html>
    <head>
    <?php include '../layouts/link.php'; ?>
    </head>
    <body class="skin-blue">
        <?php include '../layouts/header.php'; ?>
        <?php include '../layouts/navbar.php'; ?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Peminjaman
                        <small>Data peminjaman</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-bar-chart-o"></i> Home</a></li>
                        <li class="active"></i> Peminjaman</li>
                    </ol>
                </section>
                <hr>

                <section class="content">
                    <div class="row">
                        <?php
                          $query = mysqli_query($koneksi, "SELECT * FROM table_invent");
                          while($show=mysqli_fetch_array($query)){
                        ?>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                        <!-- small box -->
                        <div class="small-box bg-blue">
                            <div class="inner">
                                <h3>
                                    <?php echo $show['nama_barang']; ?>
                                </h3>
                                <p>
                                    Stock tersedia : <?php echo $show['jumlah'];?>
                                </p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-archive"></i>
                            </div>
                                <a href="#" data-target="#ModalAdd<?=$show['id_invent'];?>" data-toggle="modal" class="small-box-footer">Pinjam <?php echo $show['nama_barang'];?> <i class="fa fa-share-square-o"></i></a>
                            </div>
                        </div><!-- ./col -->
                        <?php } ?>
                    </div>
                </section>
            </aside><!-- /.right-side -->

            <?php
              $query = mysqli_query($koneksi, "SELECT * FROM table_invent");
              while($show=mysqli_fetch_array($query)){
            ?>
            <div id="ModalAdd<?=$show['id_invent'];?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Form Peminjaman</h4>
                  </div>
                  <div class="modal-body">
                    <form action="crud/proses_pinjam_barang.php" name="modal_popup" enctype="multipart/form-data" method="POST">
                      <input type="hidden" name="id_invent" value="<?php echo $show['id_invent'];?>">
                      <div class="form-group">
                        <?php if(isset($_SESSION['login_admin'])){
                          $id_petugas=$_SESSION['login_admin'];
                          $nama_petugas=$_SESSION['nama_petugas'];
                        }else if(isset($_SESSION['login_operator'])){
                          $id_petugas=$_SESSION['login_operator'];
                          $nama_petugas=$_SESSION['nama_petugas'];
                        }else if(isset($_SESSION['login_peminjam'])){
                          $id_petugas=$_SESSION['login_peminjam'];
                          $nama_petugas=$_SESSION['nama_pegawai'];
                        }
                        ?>
                        <label for="nama_petugas">Nama Peminjam</label>
                        <input type="text" name="nama_petugas" class="form-control" autocomplete="off" disabled value="<?php echo $nama_petugas?>">
                        <input type="hidden" name="id_petugas" value="<?php echo $id_petugas?>">
                      </div>
                      <div class="form-group">
                        <label>Nama Barang</label>
                        <input type="text" name="nama_barang" class="form-control" autocomplete="off" disabled value="<?php echo $show['nama_barang'];?>">
                      </div>
                      <div class="form-group">
                        <label for="jumlah">Jumlah Barang</label>
                        <input type="number" name="jumlah" class="form-control" autocomplete="off" max="<?php echo $show['jumlah'];?>" value="1" min="1"/>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
                        <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i> Batal</button>
                      </div>
                    </form>
                  </div>   
                </div>
              </div>
            </div>
            <?php 
            }
            ?>
        </div><!-- ./wrapper -->
        <?php include '../layouts/script.php'; ?>
        <!-- jQuery 3.2.1 -->
        <script src="../assets/jquery-3.2.1.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="../assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="../assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- Memanggil Autocomplete.js -->
        <script src="assets/js/jquery.autocomplete.min.js"></script>
        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
    </body>
</html>
<?php
}else{
  if(isset($_SESSION['login_peminjam'])){
    echo"<script>window.location.assign('../login2.php');</script>";
  }else{
    echo"<script>window.location.assign('../login.php');</script>";
  }
}
?>