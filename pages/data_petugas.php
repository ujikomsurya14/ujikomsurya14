<!-- Cek apakah sudah login -->
<?php
  include '../config/koneksi.php';
  session_start();
  if(isset($_SESSION['login_admin']) || isset($_SESSION['login_operator']) || isset($_SESSION['login_peminjam']) ){
?>
<!DOCTYPE html>
<html>
    <head>
    <?php include '../layouts/link.php'; ?>
    </head>
    <body class="skin-blue">
        <?php include '../layouts/header.php'; ?>
        <?php include '../layouts/navbar.php'; ?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Petugas
                        <small>Data Petugas</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-bar-chart-o"></i> Home</a></li>
                        <li class="active"></i> Petugas</li>
                    </ol>
                </section>
                <hr>
                <!-- Semua Konten -->
                <section class="content">
                        <!-- box -->
                        <div class="box box-solid box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-table"></i> Tabel Data Petugas</h3>
                                <!-- Alat box -->
                                <div class="pull-right box-tools">
                                    <button class="btn btn-primary btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-primary btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div><!-- /. alat -->
                            </div><!-- box-header -->
                            <div class="box-body">
                                <div class="box-tools pull-left">
                                    <a href="#" data-target="#ModalAdd" data-toggle="modal" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Petugas</a>
                                </div><br><br>
                                <div class="table-responsive">
                                  <table id="example1" class="table table-bordered table-hover">
                                      <thead>
                                      <tr>
                                          <th class="text-center tableNumber">No.</th>
                                          <th>Nama Lengkap</th>
                                          <th>Email</th>
                                          <th>Username</th>
                                          <th>Password</th>
                                          <th>Level</th>
                                          <th class="text-center tableOpsi">Opsi</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <?php
                                        include("../config/koneksi.php");
                                        $no=0;
                                        $query = mysqli_query($koneksi,"SELECT * FROM table_petugas") or die (mysqli_error());
                                        if (mysqli_num_rows($query) == 0) {
                                            echo '<tr><td class="text-center" colspan="6">Tidak ada Data!</td></tr>';
                                        }else{
                                            while ($data = mysqli_fetch_array($query)) {
                                            $no++;
                                      ?>
                                      <tr>
                                          <td class="text-center"><?php echo $no; ?></td>          
                                          <td><?php echo $data['nama_petugas']; ?></td>          
                                          <td><?php echo $data['email']; ?></td>          
                                          <td><?php echo $data['username']; ?></td>
                                          <td><?php echo $data['password']; ?></td>          
                                          <td><?php echo $data['level']; ?></td>          
                                          <td class="text-center">
                                            <a class="btn btn-primary btn-xs open_modal" id='<?php echo $data['id_petugas']; ?>'><i class="fa fa-edit"></i> Edit</a>
                                            <a class="btn btn-danger btn-xs" onclick="confirm_modal('crud/proses_hapus_petugas.php?&id_petugas=<?php echo $data['id_petugas']; ?>');"><i class="fa fa-trash-o"></i> Hapus</a>
                                          </td>
                                      </tr>
                                      <?php
                                      }
                                      }
                                      ?>
                                    </tbody>  
                                  </table>
                                </div>
                              </div>
                          <!-- /.box-body -->
                        </div><!-- /.box -->
                </section><!-- /.content (semua konten) -->
              <!-- CRUD MODAL -->
              <!-- Modal Popup untuk Add--> 
              <div id="ModalAdd" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      <h4 class="modal-title" id="myModalLabel">Form Tambah Petugas</h4>
                    </div>
                    <div class="modal-body">
                      <form action="crud/proses_tambah_petugas.php" name="modal_popup" enctype="multipart/form-data" method="POST">
                        <input type="hidden" name="status" value="1">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label for="nama_petugas">Nama Lengkap</label>
                            <input type="text" name="nama_petugas" class="form-control" autocomplete="off" required/>
                          </div>
                          <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control" autocomplete="off" required/>
                          </div>
                          <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" name="username" class="form-control" autocomplete="off" required/>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control" autocomplete="off" required/>
                          </div>
                          <div class="form-group">
                            <label>Level</label>
                            <select name="level" class="form-control" required>
                              <option selected="selected" hidden disabled="">Pilih Level</option>
                              <option value="Admin">Admin</option>
                              <option value="Operator">Operator</option>
                            </select>
                          </div>       
                        </div>
                        <div class="modal-footer">
                          <div class="col-sm-12">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
                            <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i> Batal</button>
                          </div>
                        </div>
                      </form>
                    </div>   
                  </div>
                </div>
              </div>
              <!-- Modal Popup untuk Edit--> 
              <div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
              <!-- Modal Popup untuk Delete--> 
              <div class="modal fade" id="modal_delete">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title">Apa anda yakin ingin menghapusnya?</h4>
                    </div>
                    <div class="modal-footer" style="padding: 15px !important;">
                        <a class="btn btn-warning" id="delete_link"><i class="fa fa-trash-o"></i> Hapus</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- END CRUD MODAL -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <?php include '../layouts/script.php'; ?>
        <!-- Javascript untuk popup modal Edit--> 
        <script type="text/javascript">
          $(document).ready(function () {
          $(".open_modal").click(function(e) {
              var m = $(this).attr("id");
              $.ajax({
                    url: "crud/form_edit_petugas.php",
                    type: "GET",
                    data : {id_petugas: m,},
                    success: function (ajaxData){
                      $("#ModalEdit").html(ajaxData);
                      $("#ModalEdit").modal('show',{backdrop: 'true'});
                    }
                  });
                });
              });
        </script>
        <!-- Javascript untuk popup modal Delete--> 
        <script type="text/javascript">
            function confirm_modal(delete_url)
            {
              $('#modal_delete').modal('show', {backdrop: 'static'});
              document.getElementById('delete_link').setAttribute('url' , delete_url);
            }
            $('#delete_link').click(function(){
              window.location=$('#delete_link')[0].attributes.url.value;
            })
        </script>
        <!-- DATA TABES SCRIPT -->
        <script src="../assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="../assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>

    </body>
</html>
<?php
}else{
  if(isset($_SESSION['login_peminjam'])){
    echo"<script>window.location.assign('../login2.php');</script>";
  }else{
    echo"<script>window.location.assign('../login.php');</script>";
  }
}
?>