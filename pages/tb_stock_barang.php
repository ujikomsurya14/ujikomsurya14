<!-- Cek apakah sudah login -->
<?php
  include 'koneksi.php';
  session_start();
  if(isset($_SESSION['login_admin']) || isset($_SESSION['login_operator']) || isset($_SESSION['login_peminjam']) ){
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>SMKN 1 CIOMAS</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="assets/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- ICOM -->
        <link href='assets/img/logo.png' rel='shortcut icon'>

    </head>
    <body class="skin-blue">
        <?php
        include 'header.php';
        ?>
        <?php
        include 'navbar.php';
        ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <?php  
                    include 'koneksi.php';
                    $id_inventaris = $_GET['id_inventaris'];
                    $query = mysqli_query($koneksi,"SELECT * FROM inventaris WHERE id_inventaris='$id_inventaris'");
                    while($d = mysqli_fetch_array($query)){
                 ?>

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Tambah Stock
                        <small>Stock&nbsp;<?php echo $d['nama']; ?></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-bar-chart-o"></i> Home</a></li>
                        <li class="active"></i> Barang</li>
                    </ol>
                </section>
                <hr>
                <!-- Semua Konten -->
                <section class="content">

                        <!-- modal -->
                        <div id="modal modal">

                        </div><!-- modal -->

                    <!-- Row Tengah -->
                    <div class="col-lg-12">

                        <!-- box -->
                        <div class="box box-solid box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-list"></i>&nbsp;Tambah Stock&nbsp;<?php echo $d['nama']; ?></h3>
                                <!-- Alat box -->
                                <div class="pull-right box-tools">
                                    <button class="btn btn-primary btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-primary btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div><!-- /. alat -->
                            </div><!-- box-header -->
                            <div class="box-body">
                                <form action="proses/proses_tb_stock.php" method="POST">
                                    <input type="hidden" name="id_inventaris" value="<?php echo $d['id_inventaris']; ?>">
                                    <label for="jumlah">Jumlah</label>
                                    <input type="number" id="jumlah" autocomplete="off" name="jumlah" value="<?php echo $d['jumlah']; ?>" class="form-control" required/>
                            </div>
                            <div class="box-footer clearfix">
                                    <button type="submit" name="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                                    <a href="data_barang.php" class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;Batal</a>
                                </form>
                                <?php
                            }
                                ?>
                            </div><!-- box-footer -->
                        </div><!-- /.box -->

                    </div><!-- /.col-12 -->
                </section><!-- /.content (semua konten) -->

            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->


        <!-- jQuery 2.0.2 -->
        <script src="assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="assets/js/plugins/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="assets/js/plugins/AdminLTE/app.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="assets/js/AdminLTE/app.js" type="text/javascript"></script>
    </body>
</html>
<?php
}else{
  if(isset($_SESSION['login_peminjam'])){
    echo"<script>window.location.assign('login_peminjam.php');</script>";
  }else{
    echo"<script>window.location.assign('login.php');</script>";
  }
}
?>