<!-- Cek apakah sudah login -->
<?php
  include '../config/koneksi.php';
  session_start();
  if(isset($_SESSION['login_admin']) || isset($_SESSION['login_operator']) || isset($_SESSION['login_peminjam']) ){
?>
<!DOCTYPE html>
<html>
    <head>
    <?php include '../layouts/link.php'; ?>
    </head>
    <body class="skin-blue">
        <?php include '../layouts/header.php'; ?>
        <?php include '../layouts/navbar.php'; ?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Inventarisir<small>Data inventaris</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li class="active"><a href="#"><i class="fa fa-bar-chart-o"></i> Home</a></li>
                    </ol>
                </section>
                <section class="content">
                </section>
            </aside><!-- right-side -->
        </div><!-- ./wrapper -->
        <?php include '../layouts/script.php';?>
    </body>
</html>
<?php
}else{
  if(isset($_SESSION['login_peminjam'])){
    echo"<script>window.location.assign('../login2.php');</script>";
  }else{
    echo"<script>window.location.assign('../login.php');</script>";
  }
}
?>