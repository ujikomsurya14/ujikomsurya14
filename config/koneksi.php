<?php
    $koneksi = mysqli_connect('localhost','root','','ujikom_surya') or die ("Koneksi Gagal");
    //TIMEZONE
    date_default_timezone_set("Asia/Jakarta");
    $date= date("Y-m-d");
    // NOMOR URUT BARANG
    $query =mysqli_query($koneksi, "SELECT max(kode_barang) as maxKode FROM table_invent");
    $data = mysqli_fetch_array($query);
    $noBarang = $data['maxKode'];
    $urutBarang = (int) substr($noBarang, 9, 3);
    $urutBarang++;
    $char = "KDB";
    $bulan=substr($date, 0, 4);
    $tahun=substr($date, 8, 4);
    $kode_barang = $char .$tahun .$bulan . sprintf("%03s", $urutBarang);
?>