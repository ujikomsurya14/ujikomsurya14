<?php
    session_start();
    include 'config/koneksi.php';
    if(isset($_SESSION['login_peminjam'])){
        header("location:pages/index.php");
    }else{
?>
<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>Log in</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- ICOM -->
        <link href='assets/img/logo.png' rel='shortcut icon'>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-black">

        <div class="form-box" id="login-box">
            <div class="header">Login Peminjam</div>
            <form action="" method="post">
                <div class="body bg-gray">
                    <div align="center">
                    </div>
                    <div class="form-group">
                        <input type="text" name="nip" autocomplete="off" maxlength="16" id="nip" class="form-control" placeholder="Nomor Induk Petugas (NIP)" onkeyup="myFunction()" required/>
                    </div>
                </div>
                <div class="footer">                                                               
                    <button type="submit" name="login" class="btn bg-olive btn-block">Log In</button>
                    <a href="register.php" class="text-center">Daftar Akun</a>
                    <a href="login.php" class="pull-right">Masuk sebagai Petugas</a>
                </div>
            </form>


                <?php
                    if (isset($_POST['login'])) {
                        $nip = $_POST['nip'];
                        $select = mysqli_query($koneksi,"SELECT * FROM table_pegawai WHERE nip='$nip'");
                        $cek = mysqli_num_rows($select);
                        if($cek == 1){
                            $bagi = mysqli_fetch_array($select);
                            $_SESSION['login_peminjam']=$bagi['id_pegawai'];
                            $_SESSION['nip']=$bagi['nip'];
                            $_SESSION['nama_pegawai']=$bagi['nama_pegawai'];
                            echo"<script>alert('Anda Berhasil Login!');window.location.assign('pages/index.php');</script>";
                        }else{
                            echo"<script>alert('NIP Tidak Ada!');</script>";
                        }
                    }
                ?>

            <div class="margin text-center">
            </div>
        </div>

        <!-- jQuery 2.0.2 -->
        <script src="../../js/jquery-1.11.2.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Capital -->
        <script type="text/javascript">
            function myFunction() {
                var x = document.getElementById("nama");
                x.value = x.value.toUpperCase();
            }
        </script>        

    </body>
</html>
<?php
}
?>