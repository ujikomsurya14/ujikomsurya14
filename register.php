<?php
    session_start();
    include 'config/koneksi.php';
?>
<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>Registration</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- ICOM -->
        <link href='assets/img/logo.png' rel='shortcut icon'>
    </head>
    <body class="bg-black">

        <div class="form-box" id="login-box">
            <div class="header">Sign Up Akun Baru</div>
            <form action="" method="post">
                <div class="body bg-gray">
                    <div class="form-group">
                        <input type="text" name="nip" autocomplete="off" class="form-control" title="*Harap hafal NIP,Karna akan digunakan untuk keperluan Login Anda" maxLength="16" placeholder="Buat NIP Baru" required/>
                    </div>
                    <div class="form-group">
                        <input type="text" name="nama_pegawai" autocomplete="off" class="form-control" placeholder="Masukan Nama Lengkap" onkeyup="myFunction()" required/>
                    </div>
                    <div class="form-group">
                        <textarea name="alamat" class="form-control" placeholder="Alamat Lengkap"></textarea>
                    </div>
                </div>
                <div class="footer">
                    <button type="submit" name="register" class="btn bg-olive btn-block">Sign Up</button>
                    <a href="login2.php" class="text-center">Sudah Punya Akun?</a>
                </div>
            </form>
        </div>

                <?php
                    if (isset($_POST['register'])) {
                        $nip = $_POST['nip'];
                        $nama_pegawai = $_POST['nama_pegawai'];
                        $alamat = $_POST['alamat'];
                        $sql = "SELECT * FROM table_pegawai WHERE nip='$nip'";
                        $query = $koneksi->query($sql);
                        if($query->num_rows != 0){
                            echo "<script>alert('NIP sudah terdaftar!,gunakan NIP lain');window.location.assign('register.php');</script>";
                        } else {
                            if(!$nip || !$nama_pegawai || !$alamat){
                                echo "<script>alert('Lengkapi data yang Kosong!');window.location.assign('register.php');</script>";
                            } else{
                                $data = "INSERT into table_pegawai VALUES (NULL,'$nama_pegawai','$nip','$alamat')";
                                $simpan = $koneksi->query($data);
                                if($simpan){
                                    echo "<script>alert('Anda Berhasil Mendaftar!');window.location.assign('login2.php');</script>";
                                } else{
                                    echo "<script>alert('Anda Gagal Mendaftar!');window.location.assign('register.php');</script>";
                                }
                            }
                        }
                    }
                ?>


        <!-- jQuery 2.0.2 -->
       <script src="../../js/jquery-1.11.2.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Capital -->
        <script type="text/javascript">
            function myFunction() {
                var x = document.getElementById("nama");
                x.value = x.value.toUpperCase();
            }
        </script>        

    </body>
</html>