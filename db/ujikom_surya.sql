-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 13 Apr 2019 pada 11.57
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ujikom_surya`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_detail`
--

CREATE TABLE `table_detail` (
  `id_detail_pinjam` int(11) NOT NULL,
  `id_invent` int(11) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  `jumlah` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `table_detail`
--

INSERT INTO `table_detail` (`id_detail_pinjam`, `id_invent`, `id_peminjaman`, `jumlah`) VALUES
(34, 55, 50, '1'),
(35, 56, 51, '1'),
(36, 55, 52, '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_invent`
--

CREATE TABLE `table_invent` (
  `id_invent` int(11) NOT NULL,
  `kode_barang` varchar(200) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `kondisi_barang` varchar(50) NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `jumlah` bigint(20) NOT NULL,
  `tgl_register` date NOT NULL,
  `keterangan` text NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `table_invent`
--

INSERT INTO `table_invent` (`id_invent`, `kode_barang`, `nama_barang`, `id_jenis`, `kondisi_barang`, `id_ruang`, `jumlah`, `tgl_register`, `keterangan`, `id_petugas`, `foto`) VALUES
(55, 'KDB112019192', 'Sarung', 9, 'Baik', 17, 24, '2019-04-11', 'Tersedia', 1, ''),
(56, 'KDB112019193', 'Laptop', 4, 'Baik', 9, 12, '2019-04-11', 'Tersedia', 1, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_jenis`
--

CREATE TABLE `table_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(25) NOT NULL,
  `kode_jenis` varchar(25) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `table_jenis`
--

INSERT INTO `table_jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(4, 'Alat Elektronik', 'ELKT', 'Untuk keperluan Elektrik'),
(5, 'Alat Olahraga', 'OR', 'Untuk keperluan praktek kesehatan jasmani'),
(9, 'Alat Sholat', 'ASHLT', 'Untuk keperluan Sholat');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_pegawai`
--

CREATE TABLE `table_pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `table_pegawai`
--

INSERT INTO `table_pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `alamat`) VALUES
(16, 'Arman Maulana', '1212121212121212', 'Ciberegbeg/Kab. Bogor/Jawa Barat'),
(17, 'Soorya Fazhriel', '1234567899999999', 'jauh make helm4 bawa stnk 2 sim 5');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_peminjaman`
--

CREATE TABLE `table_peminjaman` (
  `id_peminjaman` int(11) NOT NULL,
  `tgl_pinjam` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tgl_kembali` varchar(20) NOT NULL,
  `status_peminjaman` varchar(50) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `table_peminjaman`
--

INSERT INTO `table_peminjaman` (`id_peminjaman`, `tgl_pinjam`, `tgl_kembali`, `status_peminjaman`, `id_petugas`, `id_pegawai`) VALUES
(50, '2019-04-11 18:31:40', '2019-04-12 01:31:40', 'Telah Dikembalikan', 6, 0),
(51, '2019-04-11 18:31:52', '2019-04-12 01:31:52', 'Telah Dikembalikan', 6, 0),
(52, '2019-04-11 19:39:24', '', 'Sedang Dipinjam', 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_petugas`
--

CREATE TABLE `table_petugas` (
  `id_petugas` int(11) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL,
  `user_key` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `level` enum('Admin','Operator') NOT NULL DEFAULT 'Operator'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `table_petugas`
--

INSERT INTO `table_petugas` (`id_petugas`, `nama_petugas`, `username`, `password`, `email`, `user_key`, `status`, `level`) VALUES
(7, 'Soorya Fazhriel', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'surs.fzrl14@gmail.com', '', 1, 'Admin'),
(8, 'Darma Chandra', 'operator', '4b583376b2767b923c3e1da60d10de59', 'darma.op@operator.com', '', 1, 'Operator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_ruang`
--

CREATE TABLE `table_ruang` (
  `id_ruang` int(11) NOT NULL,
  `nama_ruang` varchar(25) NOT NULL,
  `kode_ruang` varchar(25) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `table_ruang`
--

INSERT INTO `table_ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(9, 'Laboratorium RPL', 'L.RPL', 'Untuk keperluan produktif'),
(14, 'Ruang Osis', 'R.OS', 'Ruang Osis\r\n'),
(16, 'Ruang Guru', 'R.GU', 'Ruang Guru'),
(17, 'Masjid', 'MSJD', 'Masjid ini digunakan untuk beribadah Siswa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `table_detail`
--
ALTER TABLE `table_detail`
  ADD PRIMARY KEY (`id_detail_pinjam`);

--
-- Indexes for table `table_invent`
--
ALTER TABLE `table_invent`
  ADD PRIMARY KEY (`id_invent`);

--
-- Indexes for table `table_jenis`
--
ALTER TABLE `table_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `table_pegawai`
--
ALTER TABLE `table_pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `table_peminjaman`
--
ALTER TABLE `table_peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`);

--
-- Indexes for table `table_petugas`
--
ALTER TABLE `table_petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `table_ruang`
--
ALTER TABLE `table_ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `table_detail`
--
ALTER TABLE `table_detail`
  MODIFY `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `table_invent`
--
ALTER TABLE `table_invent`
  MODIFY `id_invent` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `table_jenis`
--
ALTER TABLE `table_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `table_pegawai`
--
ALTER TABLE `table_pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `table_peminjaman`
--
ALTER TABLE `table_peminjaman`
  MODIFY `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `table_petugas`
--
ALTER TABLE `table_petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `table_ruang`
--
ALTER TABLE `table_ruang`
  MODIFY `id_ruang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
